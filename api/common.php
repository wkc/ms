<?php
//session会影响缓存
//session_start();

//连接数据库.. PDO方式连接
global $pdo;
if(isset($_SERVER['HTTP_APPNAME']))
{
    //for sae
    $dbms='mysql';
    $dbName=SAE_MYSQL_DB;
    $user= SAE_MYSQL_USER;
    $pwd=SAE_MYSQL_PASS;
    $host=SAE_MYSQL_HOST_M.':'. SAE_MYSQL_PORT;
    $dsn="$dbms:host=$host;dbname=$dbName";
    $pdo=new PDO($dsn,$user,$pwd);
}
else
{
    //for 本地
    $dbms='mysql';
    $dbName='ms';
    $user='root';
    $pwd='';
    $host='localhost';
    $dsn="$dbms:host=$host;dbname=$dbName";
    $pdo=new PDO($dsn,$user,$pwd);
    $pdo->query("set names utf8;");
}



function pdoGet($sql,$pre){
	global $pdo;
	$result=$pdo->prepare($sql);
	$result->execute($pre);
	return $result->fetchAll(PDO::FETCH_ASSOC);
}


function pdoJson($sql,$pre){
	return json_encode(pdoGet($sql, $pre));
}



?>
