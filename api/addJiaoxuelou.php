<?php
session_start();
include "common.php";
if ($_SESSION['permission'] !='admin'){
    die(json_encode(array("state" => "error", "msg" => "没有权限")));
}
try {

    $rawpostdata = file_get_contents("php://input");
    //  var_dump($rawpostdata);
    $post = json_decode($rawpostdata, true);
    // var_dump($post);
    $sql="DELETE FROM `ms`.`jiaoxuelou` WHERE 1 ";
    pdoGet($sql,NULL);
    foreach ($post as  $value) {
        // echo $value['id'],$value['name'],$value['count'];
        $sql = "INSERT INTO `ms`.`jiaoxuelou` (`id`, `name`, `count`) VALUES (?,?,?);";
        pdoGet($sql,array($value['id'],$value['name'],$value['count']));
    }
    die(json_encode(array("state" => "ok", "msg" => "修改成功")));
} catch(Exception $e) {
    die(json_encode(array("state" => "error", "msg" => "未知错误")));
}
?>

