<?php
session_start();
include "common.php";
try {

	$rawpostdata = file_get_contents("php://input");
	//	var_dump($rawpostdata);
	$post = json_decode($rawpostdata, true);
	if (count($post) != 2)
		die(json_encode(array("state" => "error", "msg" => "请输入完整")));
	$pre = array($post["username"]);
	//	var_dump($pre);
	$sql = "SELECT * FROM `user` WHERE `username` = ? ";
	$result = pdoGet($sql, $pre);
	if (count($result) == 0) {
		die(json_encode(array("state" => "error", "msg" => "用户不存在")));
	}

	if ($result[0]['password'] == $post["password"]) {
		session_regenerate_id();
		echo json_encode(array("state" => "success", "msg" => "登录成功","username"=>$result[0]["username"],"nickname"=>$result[0]["nickname"]));
		$_SESSION["isLogin"]=true;
		$_SESSION["username"]=$result[0]["username"];
		$_SESSION["nickname"]=$result[0]["nickname"];
		$_SESSION['permission']=$result[0]['permission'];
	} else {
		echo json_encode(array("state" => "error", "msg" => "密码错误"));
	}
} catch(Exception $e) {
	die(json_encode(array("state" => "error", "msg" => "未知错误")));
}
?>


