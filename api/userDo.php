<?php
session_start();
include "common.php";
if ($_SESSION['permission'] !='admin'){
    die(json_encode(array("state" => "error", "msg" => "没有权限")));
}
try {

    $rawpostdata = file_get_contents("php://input");
    //  var_dump($rawpostdata);
    $post = json_decode($rawpostdata, true);

    if ($post['op'] == 'userAdd'){
        $sql = 'INSERT INTO USER( username, PASSWORD ) value(?,?)';
        $r=pdoGet($sql,array($post['username'],$post['password']));
        echo json_encode(array("state" => "success", "msg" => "增加成功"));
    }
    else if ($post['op'] == 'userDel'){
        $sql = "DELETE FROM `ms`.`user` WHERE `user`.`username` = ? ";
        pdoGet($sql,array($post['username']));
        echo json_encode(array("state" => "success", "msg" => "删除成功"));
    }
    else if ($post['op'] == 'userPassReset'){
        $sql = "UPDATE `ms`.`user` SET `password` = ? WHERE `user`.`username` = ?;";
        pdoGet($sql,array($post['password'],$post['username']));
        echo json_encode(array("state" => "success", "msg" => "修改成功"));
    }
    else if ($post['op'] == 'setAdmin'){
        $sql = "UPDATE `ms`.`user` SET `permission` = 'admin' WHERE `user`.`username` = ?;";
        pdoGet($sql,array($post['username']));
        echo json_encode(array("state" => "success", "msg" => "修改成功"));
    }
    else if ($post['op'] == 'cancelAdmin'){
        $sql = "UPDATE `ms`.`user` SET `permission` = 'user' WHERE `user`.`username` = ?;";
        pdoGet($sql,array($post['username']));
        echo json_encode(array("state" => "success", "msg" => "修改成功"));
    }

} catch(Exception $e) {

    die(json_encode(array("state" => "error", "msg" => "未知错误")));
}
?>

