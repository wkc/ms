window.print = window.console.log

window.adminCtrl = ($scope, $http, $filter) ->
  z=$scope
  #初始化函数
  $scope.init  = do ->
    $http(
      method: "GET"
      url: "api/isLogin.php"
    ).success((data, status, headers, config) ->
      if data['permission']!='admin' then document.write "你没有权限"
    ).error (data, status, headers, config) ->
      console.log "获取数据失败,请刷新页面"

    #暴露 $scope
    window.scope = angular.element(document.getElementsByTagName("body")).scope()
    $scope.jiaoxuelouNew=""
    $http(
      method: "GET"
      url: "api/jiaoxuelou.php"
    ).success((data, status, headers, config) ->
      $scope.jiaoxuelou = data
      arr=(parseInt(i.id) for i in scope.jiaoxuelou)
      $scope.jiaoxuelouIdMax=Math.max(arr...)
    ).error (data, status, headers, config) ->
      $scope.jiaoxuelou = [ "获取数据失败,请刷新页面" ]


    $http(
      method: "GET"
      url: "api/jiaoshi.php"
    ).success((data, status, headers, config) ->
      $scope.jiaoshi = data
      arr=(parseInt(i.jiaoshi_id) for i in $scope.jiaoshi)
      $scope.jiaoshiIdMax=Math.max(arr...)
    ).error (data, status, headers, config) ->
      $scope.jiaoshi = [ "获取数据失败,请刷新页面" ]
  #删除教学楼
  $scope.jiaoxuelouDel = ($index)->
    $scope.jiaoxuelou.splice($index,1)
  #添加教学楼
  $scope.jiaoxuelouAdd = ->
    if $scope.jiaoxuelouNew.length==0 then return 
    t={}
    t.count="0"
    t.name=$scope.jiaoxuelouNew
    $scope.jiaoxuelouNew=""
    t.id=$scope.jiaoxuelouIdMax+1
    $scope.jiaoxuelouIdMax++
    $scope.jiaoxuelou.push(t)

  $scope.clickJiaoxuelouSave= ->
    #alert(0);
    re=prompt("你确定要保存修改? 请在下面的输入框中输入\"继续\" ","取消")
    return if re==null 
    return if re!="继续"
    $scope.save()
  
  $scope.save = ->
     $http(
      method: "post"
      url: "api/addJiaoxuelou.php"
      data: $scope.jiaoxuelou
    ).success((data, status, headers, config) ->
      alert(data.msg)
      $scope.init()
    ).error (data, status, headers, config) ->
      $scope.init()
      
  ##############
  $scope.jiaoshiDel = (index)->
    for i,value of $scope.jiaoshi
      if value.jiaoshi_id == index 
        $scope.jiaoshi.splice(i,1)
        break

  $scope.jiaoshiAdd = ->
    if $scope.jiaoshiNew.length==0 then return 
    t={}
    t.jiaoshi_id=$scope.jiaoshiIdMax+1
    t.jiaoshi_name=$scope.jiaoshiNew
    t.count=0
    t.jiaoxuelou_id=scope.louNow.id
    $scope.jiaoshiNew=""
    $scope.jiaoshiIdMax++
    $scope.jiaoshi.push(t)

  $scope.clickJiaoshiSave= ->
    # todo
    re=prompt("你确定要保存修改? 请在下面的输入框中输入\"继续\" ","取消")
    return if re==null 
    return if re!="继续"
    #$scope.save()
  
  $scope.userDo = (op)->
    send={}
    send.op=op
    if (op=="userAdd" or op=="userPassReset" ) and $scope.username1 and $scope.password1
      send.username=$scope.username1
      send.password=$scope.password1
    else if  $scope.username2
      send.username=$scope.username2
    else return
    console.log send
    $http(
      method: "post"
      url: "api/userDo.php"
      data: send
    ).success((data, status, headers, config) ->
      if (op=="userAdd" or op=="userPassReset" )
        $scope.col1Msg=data['msg']
      else
        $scope.col2Msg=data['msg']
    ).error (data, status, headers, config) ->


