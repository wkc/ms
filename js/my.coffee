"use strict"
window.mainCtrl = ($scope, $http, $filter) ->
  window.scope=$scope
  $scope.username
  $scope.toDay = new Date()
  #选择的日期  Date类型
  $scope.login = new Object()
  #登录信息
  $scope.lookupModal = new Object()
  
  #处理日期选择
  $("#sandbox-container div").datepicker(
    todayBtn: "linked"
    language: "zh-CN"
    todayHighlight: true
  ).on "changeDate", (ev) ->
    console.log "choose", ev
    return  if typeof (ev.date) is "undefined"
    $scope.$apply ->
      $scope.toDay = Date.parse(ev.date)
      $scope.loadState()


  
  #在页面载入时初始化  1:判断是否登录 2:获取教学楼列表 教室列表
  init  = do ->
    #暴露 $scope
    window.scope = angular.element(document.getElementsByTagName("body")).scope()
    $scope.username = ""
    $scope.password = ""
    $http(
      method: "GET"
      url: "api/isLogin.php"
    ).success((data, status, headers, config) ->
      $scope.login = data
      $scope.login.isLogin = false  unless $scope.login.isLogin is true
    ).error (data, status, headers, config) ->
      console.log "获取数据失败,请刷新页面"

    $http(
      method: "GET"
      url: "api/jiaoxuelou.php"
    ).success((data, status, headers, config) ->
      $scope.jiaoxuelou = data
      $scope.louNow = $scope.jiaoxuelou[0]
      $scope.loadState()
    ).error (data, status, headers, config) ->
      $scope.jiaoxuelou = [ "获取数据失败,请刷新页面" ]

    $http(
      method: "GET"
      url: "api/jiaoshi.php"
    ).success((data, status, headers, config) ->
      $scope.jiaoshi = data
    ).error (data, status, headers, config) ->
      $scope.jiaoshi = [ "获取数据失败,请刷新页面" ]
    $("#controller").fadeIn "quickly",->
      setTimeout ->
        $("#loader").remove()
        return
      ,0
    return
  $scope.createStateArray = ->
    #       $scope.State
    arr = new Array()
    i = 0
    while i < $scope.jiaoshi.length
      arr[$scope.jiaoshi[i].jiaoshi_id] = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
      i++
    i = 0
    while i < $scope.State.length
      t = $scope.State[i]
      arr[t.jiaoshi_id][parseInt(t.occupy) - 1] = t.tips
      i++
    arr

  #载入当前教室和日期下的State. 并自动调用 createStateArray刷新页面
  $scope.loadState = ->
    $http(
      method: "GET"
      url: "api/getState.php?" + $scope.louNow.id + "-" + $filter("date")($scope.toDay, "yyyy-MM-dd")
    ).success((data, status, headers, config) ->
      $scope.State = data
      $scope.StateArray = $scope.createStateArray()
      #$("#table-animate").slideDown("1")
    ).error (data, status, headers, config) ->
      #$("#table-animate").slideDown("1")
      console.log "loadState error"


  $scope.setlouNow = (lou) ->
    $scope.louNow = lou
    $scope.louFilter = ""
    $scope.loadState()

  $scope.find = (actual, expected) ->
    return true  if typeof ($scope.louFilter) is "undefined"
    actual.jiaoshi_name.indexOf($scope.louFilter) isnt -1

  $scope.showTd = (js, i) ->
    try
      val = $scope.StateArray[js.jiaoshi_id][i]
      if typeof (val) is "string"
        console.log val
        return val

  
  #////////////////////////////////
  $scope.clickTd = (tr, td) ->
    console.log "click", tr, td
    td=td.toString()
    re = do ->
      for i of $scope.State
        t = $scope.State[i]
        return t  if t.jiaoshi_id is tr and t.occupy is td
      null
    if re
      #     查看
      $scope.modalLookupTitle = "查看信息"
      $scope.lookupModal = re
      $scope.lookupModal.jiaoshi_name = $scope.jiaoshi[$scope.lookupModal.jiaoshi_id].jiaoshi_name
      $("#lookup-modal").modal "show"
    else
      #         新增
      if $scope.login.isLogin is false
        $scope.loginPost()
        return
      $scope.modalLookupTitle = "新增信息"
      $scope.lookupModal = {}
      $scope.lookupModal.occupy = td
      $scope.lookupModal.username = $scope.login.username
      $scope.lookupModal.usernick = $scope.login.nickname
      
      for i of $scope.jiaoshi
        if $scope.jiaoshi[i].jiaoshi_id is tr.valueOf()
          t = i
          break
      $scope.lookupModal.jiaoshi_name = $scope.jiaoshi[t].jiaoshi_name
      $scope.lookupModal.jiaoshi_id = tr
      $("#lookup-modal").modal "show"

  $scope.loginPost = ->
    $("#modal-login").modal "show"
    if $scope.username.length is 0 or $scope.password.length is 0
      $scope.loginMsg = ""
      return
    $scope.loginMsg = "正在登录"
    $scope.login = new Object()
    $scope.login.isLogin = false
    $scope.login.username = ""
    $scope.login.usernick = ""
    
    $http(
      method: "post"
      data:
        username: $scope.username
        password: $scope.password

      url: "api/login.php"
    ).success((data, status, headers, config) ->
      console.log data
      $scope.loginMsg = data.msg
      if data.state is "success"
        $scope.login.isLogin = true
        $scope.login.username = data.username
        $scope.login.nickname = data.nickname
        $("#modal-login").modal "hide"
        $scope.password = ""
        $scope.loginMsg = ""
    ).error (data, status) ->
      console.log "lgoin post error"

  $scope.modalLoginKey = (event) ->
    $scope.loginPost()  if event.keyCode is 13

  $scope.logout = ->
    $scope.login.isLogin = false
    $http(
      method: "GET"
      url: "api/logout.php"
    ).success (data, status, headers, config) ->
      console.log "logout success"

  $scope.save = (todo) ->
    data = new Object()
    data.jiaoshi_id = $scope.lookupModal.jiaoshi_id
    data.occupy = $scope.lookupModal.occupy
    data.the_usage = $scope.lookupModal.the_usage
    data.tips = $scope.lookupModal.tips
    data.date = $filter("date")($scope.toDay, "yyyy-MM-dd")
    data.todo = todo
    
    #   Object {occupy: 2, username: "1205010207", usernick: "王克纯", jiaoshi_name: "一教105"}
    $http(
      method: "post"
      data: data
      url: "api/add.php"
    ).success((data, status, headers, config) ->
      console.log data
      $scope.loadState()
      $scope.lookupModal.msg = data.msg
    ).error (data, status) ->
      console.log "post error"


