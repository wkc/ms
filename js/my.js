(function() {
  "use strict";
  window.mainCtrl = function($scope, $http, $filter) {
    var init;
    window.scope = $scope;
    $scope.username;
    $scope.toDay = new Date();
    $scope.login = new Object();
    $scope.lookupModal = new Object();
    $("#sandbox-container div").datepicker({
      todayBtn: "linked",
      language: "zh-CN",
      todayHighlight: true
    }).on("changeDate", function(ev) {
      console.log("choose", ev);
      if (typeof ev.date === "undefined") {
        return;
      }
      return $scope.$apply(function() {
        $scope.toDay = Date.parse(ev.date);
        return $scope.loadState();
      });
    });
    init = (function() {
      window.scope = angular.element(document.getElementsByTagName("body")).scope();
      $scope.username = "";
      $scope.password = "";
      $http({
        method: "GET",
        url: "api/isLogin.php"
      }).success(function(data, status, headers, config) {
        $scope.login = data;
        if ($scope.login.isLogin !== true) {
          return $scope.login.isLogin = false;
        }
      }).error(function(data, status, headers, config) {
        return console.log("获取数据失败,请刷新页面");
      });
      $http({
        method: "GET",
        url: "api/jiaoxuelou.php"
      }).success(function(data, status, headers, config) {
        $scope.jiaoxuelou = data;
        $scope.louNow = $scope.jiaoxuelou[0];
        return $scope.loadState();
      }).error(function(data, status, headers, config) {
        return $scope.jiaoxuelou = ["获取数据失败,请刷新页面"];
      });
      $http({
        method: "GET",
        url: "api/jiaoshi.php"
      }).success(function(data, status, headers, config) {
        return $scope.jiaoshi = data;
      }).error(function(data, status, headers, config) {
        return $scope.jiaoshi = ["获取数据失败,请刷新页面"];
      });
      $("#controller").fadeIn("quickly", function() {
        return setTimeout(function() {
          $("#loader").remove();
        }, 0);
      });
    })();
    $scope.createStateArray = function() {
      var arr, i, t;
      arr = new Array();
      i = 0;
      while (i < $scope.jiaoshi.length) {
        arr[$scope.jiaoshi[i].jiaoshi_id] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        i++;
      }
      i = 0;
      while (i < $scope.State.length) {
        t = $scope.State[i];
        arr[t.jiaoshi_id][parseInt(t.occupy) - 1] = t.tips;
        i++;
      }
      return arr;
    };
    $scope.loadState = function() {
      return $http({
        method: "GET",
        url: "api/getState.php?" + $scope.louNow.id + "-" + $filter("date")($scope.toDay, "yyyy-MM-dd")
      }).success(function(data, status, headers, config) {
        $scope.State = data;
        return $scope.StateArray = $scope.createStateArray();
      }).error(function(data, status, headers, config) {
        return console.log("loadState error");
      });
    };
    $scope.setlouNow = function(lou) {
      $scope.louNow = lou;
      $scope.louFilter = "";
      return $scope.loadState();
    };
    $scope.find = function(actual, expected) {
      if (typeof $scope.louFilter === "undefined") {
        return true;
      }
      return actual.jiaoshi_name.indexOf($scope.louFilter) !== -1;
    };
    $scope.showTd = function(js, i) {
      var val;
      try {
        val = $scope.StateArray[js.jiaoshi_id][i];
        if (typeof val === "string") {
          console.log(val);
          return val;
        }
      } catch (_error) {}
    };
    $scope.clickTd = function(tr, td) {
      var i, re, t;
      console.log("click", tr, td);
      td = td.toString();
      re = (function() {
        var i, t;
        for (i in $scope.State) {
          t = $scope.State[i];
          if (t.jiaoshi_id === tr && t.occupy === td) {
            return t;
          }
        }
        return null;
      })();
      if (re) {
        $scope.modalLookupTitle = "查看信息";
        $scope.lookupModal = re;
        $scope.lookupModal.jiaoshi_name = $scope.jiaoshi[$scope.lookupModal.jiaoshi_id].jiaoshi_name;
        return $("#lookup-modal").modal("show");
      } else {
        if ($scope.login.isLogin === false) {
          $scope.loginPost();
          return;
        }
        $scope.modalLookupTitle = "新增信息";
        $scope.lookupModal = {};
        $scope.lookupModal.occupy = td;
        $scope.lookupModal.username = $scope.login.username;
        $scope.lookupModal.usernick = $scope.login.nickname;
        for (i in $scope.jiaoshi) {
          if ($scope.jiaoshi[i].jiaoshi_id === tr.valueOf()) {
            t = i;
            break;
          }
        }
        $scope.lookupModal.jiaoshi_name = $scope.jiaoshi[t].jiaoshi_name;
        $scope.lookupModal.jiaoshi_id = tr;
        return $("#lookup-modal").modal("show");
      }
    };
    $scope.loginPost = function() {
      $("#modal-login").modal("show");
      if ($scope.username.length === 0 || $scope.password.length === 0) {
        $scope.loginMsg = "";
        return;
      }
      $scope.loginMsg = "正在登录";
      $scope.login = new Object();
      $scope.login.isLogin = false;
      $scope.login.username = "";
      $scope.login.usernick = "";
      return $http({
        method: "post",
        data: {
          username: $scope.username,
          password: $scope.password
        },
        url: "api/login.php"
      }).success(function(data, status, headers, config) {
        console.log(data);
        $scope.loginMsg = data.msg;
        if (data.state === "success") {
          $scope.login.isLogin = true;
          $scope.login.username = data.username;
          $scope.login.nickname = data.nickname;
          $("#modal-login").modal("hide");
          $scope.password = "";
          return $scope.loginMsg = "";
        }
      }).error(function(data, status) {
        return console.log("lgoin post error");
      });
    };
    $scope.modalLoginKey = function(event) {
      if (event.keyCode === 13) {
        return $scope.loginPost();
      }
    };
    $scope.logout = function() {
      $scope.login.isLogin = false;
      return $http({
        method: "GET",
        url: "api/logout.php"
      }).success(function(data, status, headers, config) {
        return console.log("logout success");
      });
    };
    return $scope.save = function(todo) {
      var data;
      data = new Object();
      data.jiaoshi_id = $scope.lookupModal.jiaoshi_id;
      data.occupy = $scope.lookupModal.occupy;
      data.the_usage = $scope.lookupModal.the_usage;
      data.tips = $scope.lookupModal.tips;
      data.date = $filter("date")($scope.toDay, "yyyy-MM-dd");
      data.todo = todo;
      return $http({
        method: "post",
        data: data,
        url: "api/add.php"
      }).success(function(data, status, headers, config) {
        console.log(data);
        $scope.loadState();
        return $scope.lookupModal.msg = data.msg;
      }).error(function(data, status) {
        return console.log("post error");
      });
    };
  };

}).call(this);
