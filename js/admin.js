(function() {
  window.print = window.console.log;

  window.adminCtrl = function($scope, $http, $filter) {
    var z;
    z = $scope;
    $scope.init = (function() {
      $http({
        method: "GET",
        url: "api/isLogin.php"
      }).success(function(data, status, headers, config) {
        if (data['permission'] !== 'admin') {
          return document.write("你没有权限");
        }
      }).error(function(data, status, headers, config) {
        return console.log("获取数据失败,请刷新页面");
      });
      window.scope = angular.element(document.getElementsByTagName("body")).scope();
      $scope.jiaoxuelouNew = "";
      $http({
        method: "GET",
        url: "api/jiaoxuelou.php"
      }).success(function(data, status, headers, config) {
        var arr, i;
        $scope.jiaoxuelou = data;
        arr = (function() {
          var _i, _len, _ref, _results;
          _ref = scope.jiaoxuelou;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            i = _ref[_i];
            _results.push(parseInt(i.id));
          }
          return _results;
        })();
        return $scope.jiaoxuelouIdMax = Math.max.apply(Math, arr);
      }).error(function(data, status, headers, config) {
        return $scope.jiaoxuelou = ["获取数据失败,请刷新页面"];
      });
      return $http({
        method: "GET",
        url: "api/jiaoshi.php"
      }).success(function(data, status, headers, config) {
        var arr, i;
        $scope.jiaoshi = data;
        arr = (function() {
          var _i, _len, _ref, _results;
          _ref = $scope.jiaoshi;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            i = _ref[_i];
            _results.push(parseInt(i.jiaoshi_id));
          }
          return _results;
        })();
        return $scope.jiaoshiIdMax = Math.max.apply(Math, arr);
      }).error(function(data, status, headers, config) {
        return $scope.jiaoshi = ["获取数据失败,请刷新页面"];
      });
    })();
    $scope.jiaoxuelouDel = function($index) {
      return $scope.jiaoxuelou.splice($index, 1);
    };
    $scope.jiaoxuelouAdd = function() {
      var t;
      if ($scope.jiaoxuelouNew.length === 0) {
        return;
      }
      t = {};
      t.count = "0";
      t.name = $scope.jiaoxuelouNew;
      $scope.jiaoxuelouNew = "";
      t.id = $scope.jiaoxuelouIdMax + 1;
      $scope.jiaoxuelouIdMax++;
      return $scope.jiaoxuelou.push(t);
    };
    $scope.clickJiaoxuelouSave = function() {
      var re;
      re = prompt("你确定要保存修改? 请在下面的输入框中输入\"继续\" ", "取消");
      if (re === null) {
        return;
      }
      if (re !== "继续") {
        return;
      }
      return $scope.save();
    };
    $scope.save = function() {
      return $http({
        method: "post",
        url: "api/addJiaoxuelou.php",
        data: $scope.jiaoxuelou
      }).success(function(data, status, headers, config) {
        alert(data.msg);
        return $scope.init();
      }).error(function(data, status, headers, config) {
        return $scope.init();
      });
    };
    $scope.jiaoshiDel = function(index) {
      var i, value, _ref, _results;
      _ref = $scope.jiaoshi;
      _results = [];
      for (i in _ref) {
        value = _ref[i];
        if (value.jiaoshi_id === index) {
          $scope.jiaoshi.splice(i, 1);
          break;
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    $scope.jiaoshiAdd = function() {
      var t;
      if ($scope.jiaoshiNew.length === 0) {
        return;
      }
      t = {};
      t.jiaoshi_id = $scope.jiaoshiIdMax + 1;
      t.jiaoshi_name = $scope.jiaoshiNew;
      t.count = 0;
      t.jiaoxuelou_id = scope.louNow.id;
      $scope.jiaoshiNew = "";
      $scope.jiaoshiIdMax++;
      return $scope.jiaoshi.push(t);
    };
    $scope.clickJiaoshiSave = function() {
      var re;
      re = prompt("你确定要保存修改? 请在下面的输入框中输入\"继续\" ", "取消");
      if (re === null) {
        return;
      }
      if (re !== "继续") {

      }
    };
    return $scope.userDo = function(op) {
      var send;
      send = {};
      send.op = op;
      if ((op === "userAdd" || op === "userPassReset") && $scope.username1 && $scope.password1) {
        send.username = $scope.username1;
        send.password = $scope.password1;
      } else if ($scope.username2) {
        send.username = $scope.username2;
      } else {
        return;
      }
      console.log(send);
      return $http({
        method: "post",
        url: "api/userDo.php",
        data: send
      }).success(function(data, status, headers, config) {
        if (op === "userAdd" || op === "userPassReset") {
          return $scope.col1Msg = data['msg'];
        } else {
          return $scope.col2Msg = data['msg'];
        }
      }).error(function(data, status, headers, config) {});
    };
  };

}).call(this);
