'use strict';

module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            jade: {
                files: ['jade/*'],
                tasks: ['jade']
            },
            html: {
                files: ['Gruntfile.js'],
                tasks: ['jsbeautifier'],
                options: {
                    // debounceDelay: 250,
                },
            },
            reload: {
                files: ['index.html', 'test.html', 'css/my.css', 'test.css'],
                // tasks: ['jsbeautifier'],
                options: {
                    livereload: true,
                },
            },
            coffee: {
                files: ['js/my.coffee', 'js/admin.coffee'],
                tasks: 'coffee'
            }
        },
        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: {
                    "index.html": ["jade/index.jade", 'jade/index-nav.jade'],
                    "test.html": "jade/test.jade",
                    "admin.html": "jade/admin.jade",
                }
            }
        },
        jsbeautifier: {
            files: ['Gruntfile.js', 'css/my.css'],
        },
        coffee: {
            compile: {
                files: {
                    'js/my.js': 'js/my.coffee',
                    'js/admin.js': 'js/admin.coffee'
                }
            },
        },
        clean: {
            build: {
                src: ['index.html', 'js/my.js']
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-jsbeautifier');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
};
